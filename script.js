const app = new Vue({
    el: '#app',
    data: () => ({
        baseSeleccionada: "10",
        octal: "",
        decimal: "",
        numero: "",
       
    }),
    methods: {
        onBaseONumeroCambiado() {
            this.convertirDeDecimalALasDemasBases(parseInt(this.numero, this.baseSeleccionada));
        },
        /**
         * convertimos primero a decimal y de ahi a
         * las demás bases
         */
        convertirDeDecimalALasDemasBases(numero) {
            if (!numero)
                return;
            
            this.octal = numero.toString("8");
            this.decimal = numero.toString("10");
           
        },
        
       
    },
    
    watch: {
        baseSeleccionada() {
            this.onBaseONumeroCambiado();
        },
        numero() {
            this.onBaseONumeroCambiado();
        }
    }
});